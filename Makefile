# Environment needed to work around "digital envelope routines::unsupported"
# error. Makes anything-older-than-bleeding-edge Webpack working with current
# NodeJS versions. See https://github.com/webpack/webpack/issues/14532
export NODE_OPTIONS = --openssl-legacy-provider

all: angular-prod

angular-prod:
	pnpm install --prefer-frozen-lockfile && npx ng build --prod

deploy: angular-prod
	git checkout -f pages || git checkout --orphan=pages
	git rm -rf .
	for fn in dist/vidmark/*; do rm -rf $$(basename $$fn) && mv $$fn . && git add $$(basename $$fn); done
	git commit -m 'Deploy Codeberg pages site'
	git branch --set-upstream-to="$$(git config "branch.main.remote")/pages"; true


.PHONY: angular-prod deploy
